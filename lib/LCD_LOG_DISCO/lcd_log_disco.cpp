#include "mbed_interface.h"
#include "lcd_log_disco.h"
 
extern "C" LCD_LOG_PUTCHAR;
 
LCDLog::LCDLog(const char* name):Stream(name)
{
}
 
void LCDLog::Init(FILE* fhandle, const char* header, const char* footer)
{
    char path[64];
    LCD_LOG_Init();
    if (header)
        Header(header);
    if (footer)
        Footer(footer);
    sprintf(path, "/%s", getName());
    freopen(path, "w", fhandle);
    mbed::mbed_set_unbuffered_stream(fhandle);
    LCD_LineColor = LCD_COLOR_GRAY;
}
 
int LCDLog::_getc()
{
    return 0;
}
 
int LCDLog::_putc(int c)
{
    return lcd_log_putc(c);
}