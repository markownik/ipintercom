/* Copyright (c) 2015 Ivaylo Iltchev www.iltchev.com, MIT License
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software
* and associated documentation files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LCD_LOG_DISCO_H
#define __LCD_LOG_DISCO_H

#include <string>
#include <Stream.h>
#include "lcd_log.h"
 
class LCDLog : public mbed::Stream {
 
public:
    LCDLog(const char *name=NULL);
 
    void Init(FILE* fhandle, const char* header = NULL, const char* footer = NULL);
 
    inline void Header(const char* text) { LCD_LOG_SetHeader((uint8_t*)text); }
    inline void Footer(const char* text) { LCD_LOG_SetFooter((uint8_t*)text); }
    inline void UpdateDisplay(void) { LCD_LOG_UpdateDisplay(); }
 
protected:
    virtual int _getc();
    virtual int _putc(int c);
};
 
#endif // __LCD_LOG_DISCO_H