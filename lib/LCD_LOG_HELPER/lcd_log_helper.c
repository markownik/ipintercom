#include "lcd_log_helper.h"

void Log(const char* format, ...)
{
	LCD_LineColor = LCD_LOG_TEXT_COLOR;
	printf("  ");
	va_list args;
	va_start(args, format);
    vprintf(format, args);
	va_end(args);
}
void LogErr(const char* format, ...)
{
	LCD_LineColor = LCD_LOG_ERROR_COLOR;
	printf("  ");
	printf("ERROR: ");
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	LCD_LineColor = LCD_LOG_DEFAULT_COLOR;
}
void LogDbg(const char* format, ...)
{
	LCD_LineColor = LCD_LOG_DEBUG_COLOR;
	printf("  ");
	printf("DEBUG: ");
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	LCD_LineColor = LCD_LOG_DEFAULT_COLOR;
}