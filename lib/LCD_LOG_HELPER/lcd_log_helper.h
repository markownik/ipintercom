#ifndef  __LCD_LOG_HELPER_H
#define  __LCD_LOG_HELPER_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdio.h>
#include <stdarg.h>
#include "lcd_log.h"

void Log(const char* format, ...);
void LogErr(const char* format, ...);
void LogDbg(const char* format, ...);

#ifdef __cplusplus
}
#endif

#endif /* __LCD_LOG_HELPER_H */




