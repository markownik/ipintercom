#ifndef  __NETW_CONF_H
#define  __NETW_CONF_H

#include <mbed.h>
#include <EthernetInterface.h>

class NetwConf
{
	public:
		EthernetInterface* Interface;
		UDPSocket* Socket;

		NetwConf(EthernetInterface* interface, UDPSocket* socket);
};

#endif