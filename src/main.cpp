#include <mbed.h>											
#include <mbed_events.h>
#include <rtos.h>
#include <EthernetInterface.h>
#include "stm32f746g_discovery.h"
#include "LCD_DISCO_F746NG.h"
#include "AUDIO_DISCO_F746NG.h"
#include "lcd_log_disco.h"
#include "lcd_log_helper.h"
#include "NetwConf.h"

#define AUDIO_BLOCK_SIZE 512										//play/record buffer size in words (uint16_t)
#define AUDIO_BLOCK_SIZE_BYTES AUDIO_BLOCK_SIZE * sizeof(uint16_t)	//play/record buffer size in bytes (uint8_t)
#define DEFAULT_PORT_NUMBER 41014									//udp socket port number
#define DEVICE_1_MACADDR "00:80:e1:25:00:38"						//hardware mac-address of device #1
#define DEVICE_2_MACADDR "00:80:e1:38:00:3d"						//hardware mac-address of device #2

static const char* gateway = "192.168.0.1";					//static ip configuration gateway
static const char* netmask = "255.255.255.0";				//static ip configuration gateway
static const char* ip_addr_1 = "192.168.0.1";				//static ip configuration address for device #1
static const char* ip_addr_2 = "192.168.0.2";				//static ip configuration address for device #2
static const char* ip_addr_self_assign = "169.254.222.165";	//self-assigned ip address used during configuration


static uint16_t audio_rec_buffer[AUDIO_BLOCK_SIZE]; 		//1kb record buffer (512 16-bit values)
static uint16_t audio_play_buffer[AUDIO_BLOCK_SIZE]; 		//1kb replay buffer (512 16-bit values)
static Thread event_thread, receive_thread;					//threads used for event queue processing, and receiving udp network packets
static AUDIO_DISCO_F746NG audio;							//audio peripherials driver library
static EthernetInterface net;								//ethernet interface
static UDPSocket socket;									//udp socket
static NetwConf config(&net, &socket);						//network configuration class used for network interface and udp socket, it is used like a simple structure
static InterruptIn button(PI_0);							//interrupt on pin PI_0 (D5)
static EventQueue queue;									//event queue
static char mac_addr[20];
static bool switch_offset;
static bool replay_started;

void buffer_reset(void)										//function used to clear out all variables and buffers before audio replay/recording can start
{
	switch_offset = false;									//set replay offset switching to 0 (false)
    memset(audio_rec_buffer, 0, AUDIO_BLOCK_SIZE_BYTES);	//fill record buffer with 0's
    memset(audio_play_buffer, 0, AUDIO_BLOCK_SIZE_BYTES);	//fill replay buffer with 0's
}

void remote_audio_toggle(void)								//function used to send remote audio replay toggle 
{
	uint8_t data[1] = {0x00};								//prepare one empty byte
	config.Socket->send(&data, 1);							//send it
	LogDbg("remote stream toggle sent!\n");
}

void audio_toggle(bool is_user_requested)					//function used to switch audio replay on/off 
{
	buffer_reset();											//clear audio buffers on every toggle

	if(replay_started)										
	{
		audio.IN_Pause();									//pause the recording
		audio.OUT_Pause();									//pause the replay
		LogDbg("audio stream paused\n");
	}
	else
	{
		audio.IN_Resume();									//resume the recording
		audio.OUT_Resume();									//resume the replay
		LogDbg("audio stream resumed\n");
	}
	replay_started = !replay_started;						//toggle the replay status

	if(is_user_requested)
		remote_audio_toggle();								//if it was requested by user (by button press), send 1-byte pause packet to the other device 
}

void init_audio(void)													//function used for audio interface initialization
{
	audio.IN_OUT_Default_Init(); 										//initialize class with default settings
	audio.IN_Record(audio_rec_buffer, AUDIO_BLOCK_SIZE); 				//start recording
    audio.OUT_SetAudioFrameSlot(CODEC_AUDIOFRAME_SLOT_02); 				//set audio frame slot
    audio.OUT_Play(audio_play_buffer, AUDIO_BLOCK_SIZE_BYTES);			//start playback
	replay_started = true; 												//set replay_started to correct value
	audio_toggle(false); 												//pause the replay and recording
	LogDbg("audio init done\n");
}

int init_network_interface()															//function used for network interface initialization
{
	config.Interface->set_dhcp(false);													//turn off dhcp
	config.Interface->set_network(ip_addr_self_assign, netmask, ip_addr_self_assign);	//set network configuration to self-assign address, to temporarly enable the interface, and read the mac address of the device

	if(0 != config.Interface->connect()) 												//enable the interface, timeout is 15seconds
	{
		LogErr("error connecting with self-assigned\n");
		return -1;
	}

	if(config.Interface->get_mac_address() != NULL)										//check if it is possible to get mac address
	{
		strcpy(mac_addr, config.Interface->get_mac_address());							//assign mac address to variable mac_addr
		LogDbg("mac: %s\n", mac_addr);
	}

	config.Interface->disconnect();														//turn off the interface to set correct ip address configuration

	const char* ip_addr = ip_addr_self_assign;
	const char* remote_addr = ip_addr_self_assign;

	if(strncmp(mac_addr, DEVICE_1_MACADDR, 18) == 0)									//assign ip address based on mac address
	{																					//
		ip_addr = ip_addr_1;															//
		remote_addr = ip_addr_2;														//
	}																					//

	if(strncmp(mac_addr, DEVICE_2_MACADDR, 18) == 0)									//
	{																					//
		ip_addr = ip_addr_2;															//
		remote_addr = ip_addr_1;														//
	}																					//

	if(ip_addr == remote_addr) 															//compare pointers, if the ip_addr == remote_addr (both are equal to ip_addr_self_assign)
	{																					//it means the board mac address is incompatible
		LogErr("could not detect correct device mac address!\n");
		return -1;
	}
	
	config.Interface->set_network(ip_addr, netmask, gateway);							//set the new ip configuration

	if(0 != config.Interface->connect()) 												//enable the interface with new settings
	{
		LogErr("error connecting with ip\n");
		return -1;
	}

	LogDbg("ip: %s\n", ip_addr);

	if(config.Socket->open(config.Interface) != 0) 										//open the udp socket using the interface
	{	
		LogErr("could not open network socket!\n");
		return -1;
	}
		
	if(config.Socket->bind(DEFAULT_PORT_NUMBER) != 0) 									//bind the socket to the defined port number
	{
		LogErr("could not bind to port %d!\n", DEFAULT_PORT_NUMBER);
		return -1;
	}

	SocketAddress remoteDevice(remote_addr, DEFAULT_PORT_NUMBER);						//setup remote deivce socket address

	if(config.Socket->connect(remoteDevice) != 0) 										//connect() method does not really perform a connection on udp socket, it just sets the remote address for receiving and sending
	{
		LogErr("could not connect to remote device!\n");
		return -1;
	}
	
	config.Socket->set_blocking(true);													//set the receive() method of udp socket to blocking mode (waits for incoming bytes)

	LogDbg("network config done\n");
	return 0;
}

void init_lcd(void)													//function used to initialize lcd 
{
	LCD_DISCO_F746NG* lcd = new LCD_DISCO_F746NG();										
	lcd->Init(); 													//initialize the lcd
	lcd->LayerDefaultInit(1, LCD_FB_START_ADDRESS); 					//initialize the lcd layer of index 1 with default parameters
	lcd->SelectLayer(1); 											//set active lcd layer
	lcd->SetFont(&LCD_DEFAULT_FONT);									//set font used when displaying characters

	LCDLog* lcdlog = new LCDLog("lcdlog");										//enable lcd logging class
	lcdlog->Init(stdout, "IP-Intercom PoC", "STM32746NG-DISCOVERY");	//initialize lcd logging with stdout stream, given header and footer

	Log("\n");
	LogDbg("lcd config done\n");
}

void receive_thread_worker()																									//function running in transmit_thread
{
	LogDbg("receive_thread started!\n");

	while(true) 
	{	
		if(config.Socket->recv(audio_play_buffer + (AUDIO_BLOCK_SIZE / 2) * switch_offset, AUDIO_BLOCK_SIZE_BYTES / 2) == 1)	//check received packet length
		{
			LogDbg("remote audio toggle received\n");
			audio_toggle(false);																								//as it was remote request to turn off audio (packet lenght == 1), process the toggle with parameter set to false
		}
		else
		{
			switch_offset = !switch_offset;																						//it was a normal length audio packet, switch the buffer offset (0 or 1)
		}
	}
}

void button_pressed_out_irq(void)			//function used to handle button press outside of irq context (in the main context of event queue processing thread)
{
	audio_toggle(true);						//toggle the audio processing, as requested by user pressing the button (parameter = true)
}

void button_pressed_in_irq(void)			//function used to handle button interrupt
{
	queue.call(&button_pressed_out_irq);	//dispatch the function call to event queue
}

int main() 
{
	init_lcd();																//init lcd

	if(init_network_interface() < 0) 										//wait for network interface setup
	{ 
		wait_ms(1500);														//delay the reset to display error to the user
		NVIC_SystemReset();													//as this is critical, reset the device
	}						

	init_audio();															//init audio

	event_thread.start(callback(&queue, &EventQueue::dispatch_forever));	//start event queue processing thread

	button.rise(&button_pressed_in_irq);									//setup the button interrupt to rising edge and callback to button_pressed_in_irq function

	receive_thread.start(callback(receive_thread_worker));					//start ethernet interface socket listener thread

	event_thread.join(); 													//wait indefinitely for event queue thread to terminate
}

void buffer_rec_half(void)
{
	config.Socket->send(audio_rec_buffer, AUDIO_BLOCK_SIZE_BYTES / 2);							//send first half of the record buffer to the other device
}

void buffer_rec_full(void)
{
	config.Socket->send(audio_rec_buffer + AUDIO_BLOCK_SIZE / 2, AUDIO_BLOCK_SIZE_BYTES / 2);	//send second half of the record buffer to the other device
}

void BSP_AUDIO_IN_TransferComplete_CallBack(void) 		//callback function that manages the DMA Transfer complete interrupt
{ 
	queue.call(&buffer_rec_full); 						//dispatch the function call to event queue
}
 
void BSP_AUDIO_IN_HalfTransfer_CallBack(void) 			//callback function that manages the DMA Half Transfer complete interrupt
{ 
	queue.call(&buffer_rec_half);						//dispatch the function call to event queue
}